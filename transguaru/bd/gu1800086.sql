-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 26-Nov-2019 às 16:06
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `gu1800086`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `example_4`
--

CREATE TABLE `example_4` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `url` varchar(250) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `example_4`
--

INSERT INTO `example_4` (`id`, `title`, `url`, `priority`) VALUES
(204, 'teste 1', '7d4da-c2.jpg', NULL),
(198, 'teste 2', '49f73-c1.jpg', NULL),
(199, 'teste 3', '84990-c2.jpg', NULL),
(200, 'teste 4', 'c3cd4-c3.jpg', NULL),
(201, 'teste 5', '0ed20-c4.jpg', NULL),
(205, 'teste 6', 'b9b25-c6.jpg', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp(),
  `nome` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `news`
--

INSERT INTO `news` (`id`, `data`, `nome`, `email`) VALUES
(33, '2019-11-26 13:22:16', 'pedro', 'pedrozambao@hotmail.com'),
(35, '2019-11-26 13:39:58', 'pedro', 'pedrozambao@hot.com'),
(36, '2019-11-26 14:02:01', 'pedro', 'pedrozambao3@hotmail.com'),
(37, '2019-11-26 14:02:08', 'pedro', 'pedrozambao3@hotmail.com'),
(38, '2019-11-26 14:02:11', 'pedro', 'pedrozambao@hotmail.com'),
(39, '2019-11-26 14:04:56', 'pedro', 'pedrozambao@hotmail.com'),
(40, '2019-11-26 14:05:00', 'pedro', 'pedrozambao@hotmail.com'),
(41, '2019-11-26 14:14:21', 'pedro', 'pedrozambao3@hotmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT current_timestamp(),
  `nome` varchar(60) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(11) DEFAULT NULL,
  `partida` varchar(80) NOT NULL,
  `chegada` varchar(80) NOT NULL,
  `detalhes` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id`, `data`, `nome`, `email`, `telefone`, `partida`, `chegada`, `detalhes`) VALUES
(13, '2019-11-26 13:22:30', 'pedro henrique zambão leite', 'pedrozambao3@hotmail.com', '11959526768', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'detalhamento testegfdssssssssssssssssssss'),
(14, '2019-11-26 13:22:30', 'Pedro Zambão', 'pedrozambao2@hotmail.com', '11959526768', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'detalhe teste '),
(15, '2019-11-26 14:05:23', 'pedro', 'pedrozambao@hotmail.com', '11959526768', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'detalhamento teste'),
(16, '2019-11-26 14:06:14', 'pedro', 'pedrozambao@hotmail.com', '11959526768', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'detalhamento teste');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `example_4`
--
ALTER TABLE `example_4`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `example_4`
--
ALTER TABLE `example_4`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT de tabela `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT de tabela `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
