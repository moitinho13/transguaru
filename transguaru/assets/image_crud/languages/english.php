<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['upload_button'] = 'Envia aqui sua imagem';
$lang['upload-drop-area'] = 'Arraste imagens aqui para fazer o upload';
$lang['upload-cancel'] = 'Cancelar';
$lang['upload-failed'] = 'Falha';

$lang['loading'] = 'Carregando, aguarde um momento...';
$lang['deleting'] = 'Excluindo, aguarde um momento...';
$lang['saving_title'] = 'Salvando título ...';

$lang['list_delete'] = 'Excluir';
$lang['alert_delete'] = 'Você realmente quer excluir esta imagem ?';
