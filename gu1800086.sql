-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 15-Dez-2019 às 00:09
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `gu1800086`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `crudgal`
--

CREATE TABLE `crudgal` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `url` varchar(250) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `crudgal`
--

INSERT INTO `crudgal` (`id`, `title`, `url`, `priority`) VALUES
(204, 'Teste', '7d4da-c2.jpg', NULL),
(198, 'Teste1', '49f73-c1.jpg', NULL),
(199, 'Teste2', '84990-c2.jpg', NULL),
(200, 'Teste 3', 'c3cd4-c3.jpg', NULL),
(201, 'Teste 4', '0ed20-c4.jpg', NULL),
(205, 'Teste 5', 'b9b25-c6.jpg', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `criado_em` timestamp NOT NULL DEFAULT current_timestamp(),
  `nome` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `news`
--

INSERT INTO `news` (`id`, `criado_em`, `nome`, `email`) VALUES
(53, '2019-11-25 20:05:31', 'pedro', 'pedro@mail.com'),
(56, '2019-11-25 23:55:17', 'Andreia', 'Andreiazambao@hotmail.com'),
(57, '2019-11-29 18:17:38', 'Henrique', 'henrique.m16@hotmail.com'),
(58, '2019-12-02 16:07:20', 'pedro', 'pedrozambao@Hotmail.com'),
(59, '2019-12-09 16:55:29', 'Karine Santos', 'santoskarine.s17@gmail.com');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pessoa`
--

CREATE TABLE `pessoa` (
  `id` int(11) NOT NULL,
  `criado_em` timestamp NOT NULL DEFAULT current_timestamp(),
  `nome` varchar(60) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telefone` varchar(11) DEFAULT NULL,
  `partida` varchar(80) NOT NULL,
  `chegada` varchar(80) NOT NULL,
  `detalhes` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pessoa`
--

INSERT INTO `pessoa` (`id`, `criado_em`, `nome`, `email`, `telefone`, `partida`, `chegada`, `detalhes`) VALUES
(14, '2019-11-25 19:19:32', 'Pedro Zambão', 'pedrozambao2@hotmail.com', '11959526768', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'detalhe teste '),
(17, '2019-11-25 19:20:01', 'pedro', 'pedro@hotmail.com', '11959526768', 'asçdfjjfklsdaj', 'sadflçfjsadklsfadjkl', 'sadflçjksdfajkçlsadfkç'),
(18, '2019-11-25 19:23:49', 'asdfasdf', 'asdfdsaf@mail.com', '11959526768', 'asdfasdf', 'asdffdsa', 'sadffdsf'),
(19, '2019-11-25 20:06:34', 'pedro', 'pedro@mail.com', '11959526768', 'Rua Yara, Jardim Anny, Guarulhos - SP', 'Av. Salgado Filho, 3501 - Centro, Guarulhos - SP', 'detalhamento teste'),
(20, '2019-11-25 23:57:23', 'Andreia', 'Andreiazambao@hotmail.com', '11959526764', 'Rua Yara n22 vila Anny guarulhos sp', 'Conselheiro Crispiniano, centro, guarulhos, sp', '700kg pallets'),
(21, '2019-12-09 17:01:01', 'Karine Santos', 'santoskarine.s17@gmail.com', '11967449884', 'RUA FLORAI', 'RUA ANGELO CALDINI', 'Só pra não ficar vazio, tlg kkkkkkkkkk'),
(22, '2019-12-14 18:27:04', 'Karine Santos', 'santoskarine.s17@gmail.com', '11967449483', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'pequeno'),
(23, '2019-12-14 18:29:03', 'Andreia', 'Andreiazambao@hotmail.com', '11953391584', 'R. Arminda de Lima, 57 - Vila Progresso, Guarulhos - SP', 'Av. Salgado Filho, 3501-Centro, Guarulhos-SP', 'medio'),
(24, '2019-12-14 18:29:35', 'Pedro', 'pedrozambao@hotmail.com', '11959526768', 'Rua yara, 22-Vila anny, Guarulhos-SP', 'Av. Salgado Filho, 3501 - Centro, Guarulhos - SP', 'grande');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `crudgal`
--
ALTER TABLE `crudgal`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `pessoa`
--
ALTER TABLE `pessoa`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `crudgal`
--
ALTER TABLE `crudgal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT de tabela `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de tabela `pessoa`
--
ALTER TABLE `pessoa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
